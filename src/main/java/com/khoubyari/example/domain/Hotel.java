package com.khoubyari.example.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.time.ZonedDateTime;

/*
 * a simple domain entity doubling as a DTO
 */
@Entity
@Table(name = "hotel")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Hotel {

    @Id
    @GeneratedValue()
    private long id;

    @Column(nullable = false)
    private String name;

    @Column()
    private String description;

    @Column()
    String city;

    @Column()
    private int rating;

    @Column
    private ZonedDateTime establishedDate;

    public Hotel() {
    }

    public Hotel(String name, String description, int rating, ZonedDateTime establishedDate) {
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.establishedDate = establishedDate;
    }

    public long getId() {
        return this.id;
    }

    // for tests ONLY
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ZonedDateTime getEstablishedDate() {
        return establishedDate;
    }

    public void setEstablishedDate(ZonedDateTime establishedDate) {
        this.establishedDate = establishedDate;
    }

    @Override
    public String toString() {
        return "Hotel {" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", city='" + city + '\'' +
                ", rating=" + rating + '\'' +
                ", establishedDate=" + establishedDate +
                '}';
    }
}
